//countLetter(letter, sentence)
function countLetter(letter, sentence) {
  let result = 0;
  // Check first whether the letter is a single character.
  // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
  // If letter is invalid, return undefined.
  let mySentence = sentence.split("");
  if (mySentence.includes(letter)) {
    for (x = 0; x < mySentence.length; x++) {
      if (mySentence[x] === letter) {
        result += 1;
      }
    }
  } else {
    result = undefined;
  }
  console.log(result);
}

countLetter("a", "Hi, my name is henry abayan");

// isIsogram(text)
function isIsogram(text) {
  x = false;
  y = false;
  for (i = 0; i < text.length; i++) {
    textl = text.substring(0, i);
    textr = text.substring(i);
    x = textl.includes(text.charAt(i));
    y = textr.includes(text.charAt(i));
  }
  console.log(x && y);
}
isIsogram("henry"); //False
isIsogram("mississippi"); //True

//purchase(age, price)

function purchase(age, price) {
  // Return undefined for people aged below 13.
  // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
  // Return the rounded off price for people aged 22 to 64.
  // The returned value should be a string.
  if (age < 13) {
    console.log(`Your age is below 13. Returning: ${undefined}`);
  } else if (age >= 13 && age <= 21) {
    let discounted = Math.round(price);
    console.log(`Your age is ranging 13-21. Discouted price: ${discounted}`);
  } else if (age >= 22 && age <= 64) {
    let discount = price * 0.2;
    let discounted = price - discount;
    console.log(
      `Your age is ranging 22-64. Discouted price: ${Math.round(discounted)}`
    );
    return;
  }
}
purchase(12, 100);
purchase(13, 100.56);
purchase(22, 101);

//findHotCategories(items)
let items = [
  { id: "tltry001", name: "soap", stocks: 14, category: "toiletries" },
  { id: "tltry002", name: "shampoo", stocks: 8, category: "toiletries" },
  { id: "tltry003", name: "tissues", stocks: 0, category: "toiletries" },
  { id: "gdgt001", name: "phone", stocks: 0, category: "gadgets" },
  { id: "gdgt002", name: "monitor", stocks: 0, category: "gadgets" },
];

function findHotCategories(items) {
  let hotCategories = [];
  // Find categories that has no more stocks.
  // The hot categories must be unique; no repeating categories.
  // The passed items array from the test are the following:
  // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
  // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
  // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
  // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
  // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
  // The expected output after processing the items array is ['toiletries', 'gadgets'].
  // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
  items.forEach((item) => {
    if (item.stocks === 0) {
      hotCategories.push(item.category);
    }
  });

  hotCategories = hotCategories.filter(
    (item, index, hotCategories) => hotCategories.indexOf(item) === index
  );
  console.log(hotCategories);
}

findHotCategories(items);

//findFlyingVoters(candidateA, candidateB)

function findFlyingVoters(candidateA, candidateB) {
  // Find voters who voted for both candidate A and candidate B.
  // The passed values from the test are the following:
  // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
  // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']
  // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
  // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
  let flyingVoters = [];
  candidateA.forEach((voters) => {
    if (candidateB.includes(voters)) {
      flyingVoters.push(voters);
    }
  });
  console.log(flyingVoters);
}

findFlyingVoters(
  ["LIWf1l", "V2hjZH", "rDmZns", "PvaRBI", "i7Xw6C", "NPhm2m"],
  ["kcUtuu", "LLeUTl", "r04Zsl", "84EqYo", "V2hjZH", "LIWf1l"]
);

// module.exports = {
//   countLetter,
//   isIsogram,
//   purchase,
//   findHotCategories,
//   findFlyingVoters,
// };
