let posts = [];
let count = 1;


const noPosts = () =>{
    document.querySelector("#div-post-entries").innerHTML = "No available post(s).";
}

// If no posts in array
if(posts.length <= 0){
    noPosts();
}


// Add Post data
document.querySelector("#form-add-post").addEventListener('submit', (e) =>{
    e.preventDefault();
    posts.push({
        id: count,
        title: document.querySelector("#txt-title").value,
        body: document.querySelector("#txt-body").value
    })
    count++;
    showPosts(posts);
    alert('Successfully added');
});

// Show posts
const showPosts = (posts) => {
    let postEntries = '';

    posts.forEach((post) => {
        postEntries+= 
        `<div id="post-${post.id}">
            <h3 id="post-title-${post.id}">${post.id} : ${post.title}</h3>
            <p id="post-body-${post.id}">${post.body}</p>
            <button onclick="editPost('${post.id}')">Edit</button>
            <button onclick="deletePost('${post.id}')">Delete</button>
        </div>`
    });
    document.querySelector("#div-post-entries").innerHTML = postEntries;
    // If no posts in array
    if(posts.length <= 0){
        noPosts();
    }
}

// Edit post
const editPost = (id) =>{
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;
}

// Update post
document.querySelector("#form-edit-post").addEventListener('submit', (e) =>{
    e.preventDefault();
    for(let i = 0; i < posts.length; i++){
        // value of post [i] is a number while document.querySelector('#txt-edit-id').value is a string
		// therefore it is necessary to convert the number to a string first
        if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
            posts[i].title = document.querySelector('#txt-edit-title').value;
            posts[i].body = document.querySelector('#txt-edit-body').value;

            showPosts(posts);
            alert('Successfully updated');
            break;
        }
    }
});

// Delete post
const deletePost = (id) =>{
    for(let i = 0; i < posts.length; i++){
        if(id === posts[i].id.toString()){
            let postIndex = posts.indexOf(posts[i]);
            let postTitle = posts[i].title;
            posts.splice(postIndex,1);
            const element = document.querySelector(`#post-${id}`);
                if (element) {
                    // remove() post element in the DOM
                    element.remove();
                    alert(`${postTitle} is successfully deleted`);
                }
                // If no posts in array
                if(posts.length <= 0){
                    noPosts();
                }
            break;
        }
    }
}
