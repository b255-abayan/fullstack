import { Navigate } from "react-router-dom";
import { useContext, useEffect } from "react";
import UserContext from "../userContext";

export default function Logout() {
  // Consume the UserContext object and destructure it to access the user state and unset function from the context provider

  const { unsetUser, setUser } = useContext(UserContext);
  unsetUser();

  useEffect(() => {
    setUser({
      id: null,
    });
  });

  return <Navigate to="/login" />;
}
