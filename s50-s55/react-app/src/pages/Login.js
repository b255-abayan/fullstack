import { Form, Button } from "react-bootstrap";
import { useEffect, useState, useContext } from "react";
import { Fragment } from "react";
import UserContext from "../userContext";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";

export default function Login(props) {
  // Allow us to consume the user context object and its properties to use for user validation
  const { user, setUser } = useContext(UserContext);

  // State hooks to store the values of the input fields
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setisActive] = useState(false);

  console.log(email);
  console.log(password);

  function login(e) {
    // Prevents page redirection via form submission
    e.preventDefault();
    fetch("http://localhost:4000/users/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (typeof (data.access !== "undefined")) {
          localStorage.setItem("token", data.access);
          retrieveUserDetails(data.access);
          Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: "Welcome to Zuitt!",
          });
        } else {
          Swal.fire({
            title: "Authentication Failed",
            icon: "error",
            text: "Check you login details and try again",
          });
        }
      });
    // Set the email of the authenticated user in the local storage
    // Syntax

    // localStorage.setItem("email", email);

    // Set the global user state to have properties obtained from local storage
    // Though access to the user information can be done via the localStorage this is necessary to update the user state which will help update the App component and rerender it to avoid refreshing the page
    // When state change components are rerendered and the AppNavBar component will be updated based on the user credentials

    // setUser({
    //   email: localStorage.getItem("email", email),
    // });

    setEmail("");
    setPassword("");
    // alert("Successful login");
  }

  const retrieveUserDetails = (token) => {
    fetch("http://localhost:4000/users/details", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  };
  // Validation to enable submit button when all fields are populated and both passwords match
  useEffect(() => {
    if (email !== "" && password !== "") {
      setisActive(true);
    } else {
      setisActive(false);
    }
  }, [email, password]);

  return user.id !== null ? (
    <Navigate to="/courses" />
  ) : (
    <Fragment>
      <h1>Login</h1>
      <Form onSubmit={(e) => login(e)}>
        <Form.Group controlId="userEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>

        <Form.Group controlId="password1">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </Form.Group>
        <br />
        {isActive ? (
          <Button variant="success" type="submit" id="submitBtn">
            Login
          </Button>
        ) : (
          <Button variant="success" type="submit" id="submitBtn" disabled>
            Login
          </Button>
        )}
      </Form>
    </Fragment>
  );
}
