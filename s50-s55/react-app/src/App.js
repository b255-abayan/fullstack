// import { Fragment } from "react";
import { Container } from "react-bootstrap";
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Routes } from "react-router-dom";
import React from "react";
import "./App.css";
import AppNavbar from "./components/AppNavbar";

import Home from "./pages/Home";
import Courses from "./pages/Courses";
import CourseView from "./components/CourseView";
import Register from "./pages/Register";
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import NotFound from "./pages/NotFound";
import { useState, useEffect } from "react";
import { UserProvider } from "./userContext";

function App() {
  // State Hook for the user state thats deficed here for a global scope
  const [user, setUser] = useState({
    // email: localStorage.getItem("email"),
    id: null,
    isAdmin: null,
  });

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user]);

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <Container fluid>
          <AppNavbar />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/courses" element={<Courses />} />
            <Route path="/courseView/:courseId" element={<CourseView />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="*" element={<NotFound />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
